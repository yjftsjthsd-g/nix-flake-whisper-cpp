{ pkgs ? import <nixpkgs> {} }:

with pkgs;

stdenv.mkDerivation rec {
	name = "whisper.cpp";
	version = "1.4.0";
	src = fetchurl {
		url = "https://github.com/ggerganov/whisper.cpp/archive/refs/tags/v${version}.tar.gz";
		hash = {
			"1.4.0" = "sha256-suNOZXdwM1hPpnaaNmzbAii8XH2oHlil6NwM6U0PtU4=";
		}.${version} or "0000000000000000000000000000000000000000000000000000";
	};
    buildInputs = [ SDL2.dev ];
    buildPhase = ''
      make main bench quantize stream
    '';
    installPhase = ''
      mkdir -p $out/bin
      cp main bench quantize stream ./models/download-ggml-model.sh $out/bin/
      mkdir -p $out/share
      cp -r samples $out/share/
    '';

	meta = {
		description = "whisper.cpp does voice-to-text";
		homepage = "https://github.com/ggerganov/whisper.cpp";
		license = "MIT";
	};
}
