
wget  -O ggml-base.en.bin https://huggingface.co/ggerganov/whisper.cpp/resolve/main/ggml-base.en.bin
./result/bin/bench  -m ./ggml-base.en.bin
./result/bin/main -m ./ggml-base.en.bin -f result/share/samples/jfk.wav




### TODO ###


# nix-flake-whisper.cpp

A nix flake to build whisper.cpp, the magical AI that turns audio into text.


## Use

Prerequisite: You'll need a flake-enabled nix system; see https://nixos.wiki/wiki/Flakes for details.

To run:
```
nix run gitlab:yjftsjthsd-g/nix-flake-whisper.cpp  # TODO not really
```


## License

This repo - a bunch of build scripts, basically - is released under the MIT
license. The actual whisper.cpp itself also appears to be under the MIT license,
but do your own homework - doubly so for any models!

